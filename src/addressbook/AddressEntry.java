package addressbook;
import java.util.Scanner;
import java.util.regex.*;

/**
 * Class containing the 8 fields to define an entry into the AddressBook.
 * (firstName, lastName, street, city, state, zip, telephone, email)
 */
public class AddressEntry {
	//Member variables
	/**
	 * First name of the entry.
	 */
	private String firstName;
	/**
	 * Last name of the entry.
	 */
	private String lastName;
	/**
	 * Street name of the entry.
	 */
	private String street;
	/**
	 * City name of the entry.
	 */
	private String city;
	/**
	 * State name of the entry.
	 */
	private String state;
	/**
	 * Zip code of the entry.
	 */
	private String zip;
	/**
	 * Telephone number of the entry.
	 */
	private String telephone;
	/**
	 * Email address of the entry.
	 */
	private String email;
	
	//Getters
	/**
	 * Getter for firstName.
	 * @return String, first name of the entry.
	 */
	public String getFirstName() { return this.firstName; }
	/**
	 * Getter for lastName
	 * @return String, last name of the entry.
	 */
	public String getLastName() { return this.lastName; }
	/**
	 * Getter for street.
	 * @return String, street name of the entry.
	 */
	public String getStreet() { return this.street; }
	/**
	 * Getter for city.
	 * @return String, city name of the entry.
	 */
	public String getCity() { return this.city; }
	/**
	 * Getter for state
	 * @return String, state name of the entry.
	 */
	public String getState() { return this.state; }
	/**
	 * Getter for zip
	 * @return String, Zip Code of the entry
	 */
	public String getZip() { return this.zip; }
	/**
	 * Getter for telephone
	 * @return String, telephone number of the entry.
	 */
	public String getTelephone() { return this.telephone; }
	/**
	 * Getter for email.
	 * @return String, email address of the entry.
	 */
	public String getEmail() { return this.email; }
	
	//Setters
	/**
	 * Setter for the firstName field
	 * @param firstName, String for the firstName to be saved.
	 */
	public void setFirstName(String firstName){ this.firstName = firstName; }
	/**
	 * Setter for the lastName field.
	 * @param lastName, String for the lastName to be saved.
	 */
	public void setLastName(String lastName){ this.lastName = lastName; }
	/**
	 * Setter for the Street field.
	 * @param street, String for the street to be saved.
	 */
	public void setStreet(String street) { this.street = street; }
	/**
	 * Setter for the City field.
	 * @param city, String for the City to be saved.
	 */
	public void setCity(String city) { this.city = city; }
	/**
	 * Setter for the State field.
	 * @param state, String for the State to be saved.
	 */
	public void setState(String state) { this.state = state; }
	/**
	 * Setter for the Zip Code field.
	 * @param zip, String for the Zip Code to be saved.
	 */
	public void setZip(String zip) { this.zip = zip; }
	/**
	 * Setter for the telephone field.
	 * @param telephone, String for telephone number to be saved.
	 */
	public void setTelephone(String telephone) { this.telephone = telephone; }
	/**
	 * Setter for Email field. Will do simple email validation.
	 * Entry will revert or be null if incorrect.
	 * @param email, String for email address to be saved.
	 */
	public void setEmail(String email) { 
		//Check for email validity
		Pattern p = Pattern.compile(".+@.+\\.[a-z]+");
		Matcher m = p.matcher(email);
		boolean match = m.matches();
		if (match)
			this.email = email; 
	}
	/**
	 * Default constructor that prompts the user for input and builds the AddressEntry
	 * from the 8 fields that the user enters.
	 */
	public AddressEntry(){
		Scanner entry = new Scanner(System.in);
		System.out.println("Enter data for entry after the prompts");
		
		System.out.print(Menu.prompt_FirstName());
		setFirstName(entry.nextLine());
		
		System.out.print(Menu.prompt_LastName());
		setLastName(entry.nextLine());
		
		System.out.print(Menu.prompt_Street());
		setStreet(entry.nextLine());
		
		System.out.print(Menu.prompt_City());
		setCity(entry.nextLine());
		
		System.out.print(Menu.prompt_State());
		setState(entry.nextLine());
		
		System.out.print(Menu.prompt_Zip());
		setZip(entry.nextLine());
		
		System.out.print(Menu.prompt_Telephone());
		setTelephone(entry.nextLine());
		
		System.out.print(Menu.prompt_Email());
		setEmail(entry.nextLine());	
	}
	
	/**
	 * Secondary constructor to build an AddressEntry if all 8 fields are known.
	 * @param firstName String containing the First Name
	 * @param lastName String containing the Last Name
	 * @param street String containing the Street
	 * @param city String containing the City
	 * @param state String containing the state
	 * @param zip String containing the zip
	 * @param telephone String containing the telephone
	 * @param email String containing the email
	 */
	public AddressEntry(String firstName, String lastName, String street, String city, String state, 
							 String zip, String telephone, String email) {
		//Set the fields from the appropriate parameters
		setFirstName(firstName);
		setLastName(lastName);
		setStreet(street);
		setCity(city);
		setState(state);
		setZip(zip);
		setTelephone(telephone);
		setEmail(email);
	}
	
	/**
	 * Method to print a formatted string to the console.
	 * @return String, formatted to print to console
	 */
	public String toString(){
		return "First Name: " + this.getFirstName() +
				"\nLast Name: " + this.getLastName() +
				"\nStreet: " + this.getStreet() +
				"\nCity: " + this.getCity() +
				"\nState: " + this.getState() +
				"\nZip: " + this.getZip() +
				"\nTelephone: " + this.getTelephone() +
				"\nEmail: " + this.getEmail();
	}
	
	/**
	 * Formats the address entry into CSV format for storing as row in a CSV.
	 * @return comma separated String with 8 fields
	 */
	public String toFile() {
		return this.getFirstName() +
				"," + this.getLastName() +
				"," + this.getStreet() +
				"," + this.getCity() +
				"," + this.getState() +
				"," + this.getZip() +
				"," + this.getTelephone() +
				"," + this.getEmail();
	}
}