package addressbook;

import static org.junit.Assert.*;
import org.junit.Test;
/**
 * Test class for Menu
 * @author Trey Freeman
 *
 */
public class MenuTest {
	/**
	 * Method that tests if all prompts from Menu Class return strings.
	 * We don't want to test the actual string though as they may change over time.
	 */
	@Test
	public void test() {
		//Test prompt_Action()
		assertTrue(Menu.prompt_Action().getClass().equals(String.class));
		//Test prompt_FileName()
		assertTrue(Menu.prompt_FileName().getClass().equals(String.class));
		//Test prompt_FirstName()
		assertTrue(Menu.prompt_FirstName().getClass().equals(String.class));
		//Test prompt_LastName()
		assertTrue(Menu.prompt_LastName().getClass().equals(String.class));
		//Test prompt_Street()
		assertTrue(Menu.prompt_Street().getClass().equals(String.class));
		//Test prompt_City()
		assertTrue(Menu.prompt_City().getClass().equals(String.class));
		//Test prompt_State()
		assertTrue(Menu.prompt_State().getClass().equals(String.class));
		//Test prompt_Zip()
		assertTrue(Menu.prompt_Zip().getClass().equals(String.class));
		//Test prompt_Telephone()
		assertTrue(Menu.prompt_Telephone().getClass().equals(String.class));
		//Test prompt_Email()
		assertTrue(Menu.prompt_Email().getClass().equals(String.class));
		//Test prompt_exitMessage()
		assertTrue(Menu.exitMessage().getClass().equals(String.class));
		//Test prompt_Remove()
		assertTrue(Menu.prompt_Remove().getClass().equals(String.class));		
	}
}