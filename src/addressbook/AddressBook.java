package addressbook;
import java.io.*;
import java.util.*;

/**
 * AddressBook class holds all AddressEntries in a list called addresses.
 * The class adding, deleting, loading, and finding AddressEntries in a 
 * user's address book.
 * @author Trey Freeman
 *	
*/
public class AddressBook {		
		/**
		 * List containing all ArrayEntry instances.
		 */
		private ArrayList<AddressEntry> addresses = new ArrayList<AddressEntry>();
				
		/**
		 * Method to load AddressEntries into the addresses list from a file. 
		 * Entries must be in CSV format with no headers.
		 * @param String filename to load entries from.
		 */
		public void loadFromFile(String filename) {
			//counter to count number of AddressEntries loaded
			int counter = 0;
			
			//Try catch block to catch errors while reading from file.
			try {
				//Open file and read it into buffer.
			    BufferedReader in = new BufferedReader(new FileReader(filename));
			    String str;
			    
			    //Read lines until end of file
			    while ((str = in.readLine()) != null){
			    	//Increment counter for each new entry
			    	counter++;
			    	
			    	//Create an array based on commas.
			    	String[] splitArray = str.split(",");
			    	
			    	//Create a new address entry using the 8 fields in the CSV.
			    	AddressEntry address = new AddressEntry(splitArray[0],splitArray[1],splitArray[2],splitArray[3],splitArray[4],
			    											splitArray[5],splitArray[6],splitArray[7]);
			    	//Add the address to the list
			    	this.addresses.add(address);
			    }
			    //Close the buffer
			    in.close();
			    //If any problems with IO catch the exception and print it for the user.
			} catch (IOException e) {
					System.out.println(e);
			}
			//Let the user know how many addresses were loaded.
			System.out.println("\n" + counter + " Addresses Loaded\n");
		}
		
		/**
		 * Method that adds and AddressEntry to the addresses list.
		 * @param address, AddressEntry to be added to the addresses list.
		 * @return boolean, true if added successfully, false if fails to add.
		 */
		boolean addAddress (AddressEntry address) {
			return this.addresses.add(address);
		}
		
		/**
		 * Method to delete an AddressEntry from addresses list.
		 * The method will prompt the user for the lastName they wish to delete.
		 * Will return indexes of items that matches user input
		 * User will then select the index of the item they want to delete.
		 * Method will the prompt for deletion. y or yes will delete the entry, anything else will
		 * bread out with no action being performed.
		 * @return boolean, true is deletion successful, false if no deletion or deletion fails.
		 */
		public boolean deleteAddress() { 
			//Prompt and get lastName to delete
			System.out.print(Menu.prompt_Remove());
			
			//Get last name and store it as a string
			Scanner in = new Scanner(System.in);
			String lastName = in.next();
			
			//counter used to return the index of the list
			int counter = 0;
			//boolean flag to display found/not found message if entry/no-entries found
			boolean hit = false;
			
			//Iterate through the addresses looking for ones that exactly match 
			for(AddressEntry address : addresses){	
				if( address.getLastName().equalsIgnoreCase(lastName) ) {
					hit = true; //Set flag if at least one name found
					//Print the index and address for user inspection
					System.out.println("\nNumber: " + counter + "\n" + address.toString());
				}
				counter++;
			}
			
			//If we fail to find a lastName that matches let the user know and return.
			if (!hit) {
				System.out.print("\nNo Matches Found.\n");
				return false;
			}
			
			//If one or more found prompt the user for the index to delete
			System.out.print("\nSelect an address to delete: ");
			int toDelete = in.nextInt(); //Capture user input
			
			//Double check their intent to delete
			System.out.print("\nAre you sure you want to delete " + "Number " + toDelete + " ? (y/n):");
			String confirmation = in.next();
			
			//If the user types yes or y then the AddressEntry is deleted from the list
			if (confirmation.equalsIgnoreCase("y") || confirmation.equalsIgnoreCase("yes")){
				this.addresses.remove(toDelete);
				return true;
			}
			return false;	
		}
		
		/**
		 * Method to list all of the AddressEntries in the addresses list
		 * @return boolean, true if elements in list, false if no elements returned.
		 */
		public boolean list() {
			//Declare instance of comparator used for comparing 2 AddressEntry objects
			ObjectComparator comparator = new ObjectComparator();
			
			//Sort the list according to lastNames
			Collections.sort(this.addresses, comparator);
			
			//Counter used to index this display of entries
			int counter = 1;
			
			//If no entries break-out and inform the user.
			if (this.addresses.size() == 0) {
				System.out.println("\nNo Entries.\n");
				return false;
			}
			
			//Iterate through all entries in addresses list
			for ( AddressEntry address: this.addresses) {
				//Print each one with an index for the user
				System.out.println("\nEntry Number: " + counter++ + "\n");
				System.out.println(address.toString());
			}
			return true;
		}
		
		/**
		 * Comparator class for comparing lastName in AddressEntry. 
		 * Used for alphabetically sorting the list by the entries' lastNames.
		 */
		private static class ObjectComparator implements Comparator<AddressEntry> {
			/**
			 * Method used to compare two address entries by lastName.
			 * @return integer used to determine which name should come first alphabetically.
			 */
		    public int compare(AddressEntry first, AddressEntry second) {
		    	//Compare lastNames
		        return first.getLastName().compareToIgnoreCase(second.getLastName());
		    }
		}
		
		/**
		 * Method to find a user specified AddressEntry in the addresses list.
		 * Entries with the equivalent last name will be displayed for the user.
		 * @return boolean, true if found, false if not found. Used for testing
		 */
		public boolean find(String lastName) {

			//Flag will get set if at least one AddressEntry is found
			boolean hit = false;
			
			//Print the AddressEntrys matching user selected last name
			System.out.println("\nUsers matching " + lastName + ":\n");
			for ( AddressEntry address: this.addresses ) {
				if ( address.getLastName().equalsIgnoreCase(lastName)) {
					hit = true;
					System.out.println(address.toString());
				}
			}
			//If no entries found then let the user know.
			if (!hit) {
				System.out.println("\nNo Entries Found.\n");
				return false;
			}
			return true;
		}
		
		/**
		 * Method to store the current state of the AddressBook to a file.
		 * Will store each AddressEntry as a comma separated row of 8 values.
		 * Format for each AddressEntry will be:
		 * firstName,lastName,street,city,state,zip,telephone,email
		 * This format is readable with the loadFromFile() method
		 */
		public void store() {
			//Prompt the user for the name of the file that they want to store the addresses list to
			System.out.print("\nEnter the name of the file you wish to save your address book to:");
			
			//Capture user input
			Scanner in = new Scanner(System.in);
			String file = in.next();
			
			//Create new writer for printing to file
			PrintWriter writer;
			
			//Try/Catch block to catch any errors while writing to the file
			try {
				writer = new PrintWriter(file, "UTF-8");
				//Print each AddressEntry to the file
				for (AddressEntry address : this.addresses) {
					writer.println(address.toFile()); //toFile() returns a comma separated string
				}
				writer.close();
				System.out.println("\nSave Sucessful.\n");
			} 
			//If a problem occurs at any time when writing to the file catch the exception and 
			//let the user know.
			catch (FileNotFoundException | UnsupportedEncodingException e) {
				System.out.println("Error occurred while writing to file.");
				e.printStackTrace();
			}				
		}
}