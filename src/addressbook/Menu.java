package addressbook;

/**
 * Contains various user prompts.
 * @author Trey Freeman
 *
 */
public class Menu {
	/**
	 * First Name prompt
	 * @return String containing the prompt for the user's First Name.
	 */
	public static String prompt_FirstName() { return "First Name: "; }
	/**
	 * Last Name prompt.
	 * @return String containing the prompt for the user's Last Name.
	 */
	public static String prompt_LastName() { return "Last Name: "; }
	/**
	 * Street prompt
	 * @return String containing the prompt for the user's Street.
	 */
	public static String prompt_Street() { return "Street: "; }
	/**
	 * City prompt
	 * @return String containing the prompt for the user's City.
	 */
	public static String prompt_City() { return "City: "; }
	/**
	 * State prompt
	 * @return String containing the prompt for the user's State.
	 */
	public static String prompt_State() { return "State: "; }
	/**
	 * Zip prompt
	 * @return String containing the prompt for the user's Zip.
	 */
	public static String prompt_Zip() { return "Zip: "; }
	/**
	 * Telephone prompt
	 * @return String containing the prompt for the user's Telephone.
	 */
	public static String prompt_Telephone() { return "Telephone: "; }
	/**
	 * Email prompt
	 * @return String containing the prompt for the user's Email.
	 */
	public static String prompt_Email() { return "Email: "; }
	
	/**
	 * Menu of options
	 * @return String containing the menu of options for the user
	 */
	public static String prompt_Action() { return "\n*****************************\n" +
												"\nPlease enter in your menu selection\n" +
												"\na) Load From File\n" +
												"\nb) Addition\n" +
												"\nc) Removal\n" +
												"\nd) Find\n" +
												"\ne) Listing\n" +
												"\nf) Save and Quit\n" +
												"\n*****************************\n";
	}
	
	/**
	 * File Name prompt
	 * @return String containing the prompt for the FileName
	 */
	public static String prompt_FileName() { return "\nEnter in FileName: "; }
	
	/**
	 * Remove prompt
	 * @return String containing the prompt for the last Name to be removed
	 */
	public static String prompt_Remove() { return "\nEnter a last name for removal.\n"; }
	/**
	 * Exit message prompt
	 * @return String containing the exit message.
	 */
	public static String exitMessage() { return "\nGoodbye.\n"; }
}