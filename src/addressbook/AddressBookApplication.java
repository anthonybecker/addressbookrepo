/*
 * Copyright (c) 1995, 2008, Oracle and/or its affiliates. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   - Neither the name of Oracle or the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */ 

package addressbook;

/* TextDemo.java requires no other files. */

import java.awt.*;
import java.awt.event.*;
import java.util.Scanner;

import javax.swing.*;


public class AddressBookApplication extends JPanel implements ActionListener {
    protected JTextField textField;
    protected JTextArea textArea;
    private final static String newline = "\n";
    private AddressBook book = new AddressBook();

    public AddressBookApplication() 
    {
        super(new GridBagLayout());

        textField = new JTextField(20);
        textField.addActionListener(this);

        textArea = new JTextArea(5, 20);
        textArea.setEditable(false);
        JScrollPane scrollPane = new JScrollPane(textArea);

        //Add Components to this panel.
        GridBagConstraints c = new GridBagConstraints();
        c.gridwidth = GridBagConstraints.REMAINDER;

        c.fill = GridBagConstraints.HORIZONTAL;
        add(textField, c);

        c.fill = GridBagConstraints.BOTH;
        c.weightx = 1.0;
        c.weighty = 1.0;
        add(scrollPane, c);
        
		JButton load = new JButton("Load");
		load.setVerticalTextPosition(AbstractButton.CENTER);
        load.setHorizontalTextPosition(AbstractButton.LEADING); //aka LEFT, for left-to-right locales
        load.setActionCommand("load");
        load.addActionListener(this);
        
        JButton add = new JButton("Add");
        add.setVerticalTextPosition(AbstractButton.CENTER);
        add.setHorizontalTextPosition(AbstractButton.LEADING);
        add.setActionCommand("add");
        add.addActionListener(this);
        
        JButton remove = new JButton("Remove");
		remove.setVerticalTextPosition(AbstractButton.CENTER);
        remove.setHorizontalTextPosition(AbstractButton.LEADING); //aka LEFT, for left-to-right locales
        remove.setActionCommand("remove");
        remove.addActionListener(this);
        
        JButton find = new JButton("Find");
        find.setVerticalTextPosition(AbstractButton.CENTER);
        find.setHorizontalTextPosition(AbstractButton.LEADING);
        find.setActionCommand("find");
        find.addActionListener(this);
        
        JButton list = new JButton("List");
		list.setVerticalTextPosition(AbstractButton.CENTER);
        list.setHorizontalTextPosition(AbstractButton.LEADING); //aka LEFT, for left-to-right locales
        list.setActionCommand("list");
        list.addActionListener(this);
        
        JButton exit = new JButton("Exit");
        exit.setVerticalTextPosition(AbstractButton.CENTER);
        exit.setHorizontalTextPosition(AbstractButton.LEADING);
        exit.setActionCommand("exit");
        exit.addActionListener(this);
        
        add(load);
        add(add);
        add(remove);
        add(find);
        add(list);
        add(exit);
    }

    public void actionPerformed(ActionEvent evt) 
    {
    	if ("load".equals(evt.getActionCommand()))
    	{
			JPanel panel = new JPanel();
			JLabel label = new JLabel("Enter filename:");
			JTextField text = new JTextField(10);
			panel.add(label);
			panel.add(text);
			String[] options = new String[]{"OK", "Cancel"};
			int option = JOptionPane.showOptionDialog(null, panel, "The title",
			                         JOptionPane.NO_OPTION, JOptionPane.PLAIN_MESSAGE,
			                         null, options, options[1]);
			if(option == 0) // pressing OK button
			{
			    String filename = text.getText();
			    book.loadFromFile(filename);
			}
    	}
    	else if ("add".equals(evt.getActionCommand()))
    	{
    		JPanel panel = new JPanel();
			
    		JLabel 		label_FN = new JLabel("First name:");
			JTextField 	text_FN = new JTextField(10);
			panel.add(label_FN);
			panel.add(text_FN);
			
			JLabel label_LN = new JLabel("Last name:");
			JTextField text_LN = new JTextField(10);
			panel.add(label_LN);
			panel.add(text_LN);
			
			JLabel label_Str = new JLabel("Street:");
			JTextField text_Str = new JTextField(10);
			panel.add(label_Str);
			panel.add(text_Str);
			
			JLabel label_Ct = new JLabel("City:");
			JTextField text_Ct = new JTextField(10);
			panel.add(label_Ct);
			panel.add(text_Ct);
			
			JLabel label_Sta = new JLabel("State:");
			JTextField text_Sta = new JTextField(10);
			panel.add(label_Sta);
			panel.add(text_Sta);
			
			JLabel label_Zip = new JLabel("Zip:");
			JTextField text_Zip = new JTextField(10);
			panel.add(label_Zip);
			panel.add(text_Zip);
			
			JLabel label_Tel = new JLabel("Telephone:");
			JTextField text_Tel = new JTextField(10);
			panel.add(label_Tel);
			panel.add(text_Tel);
			
			JLabel label_Em = new JLabel("Email");
			JTextField text_Em = new JTextField(10);
			panel.add(label_Em);
			panel.add(text_Em);
			
			String[] options = new String[]{"OK", "Cancel"};
			int option = JOptionPane.showOptionDialog(null, panel, "Add Entry",
			                         JOptionPane.NO_OPTION, JOptionPane.PLAIN_MESSAGE,
			                         null, options, options[1]);
			if(option == 0) // pressing OK button
			{
			    String fn = text_FN.getText();
			    String ln = text_LN.getText();
			    String str = text_Str.getText();
			    String ct = text_Ct.getText();
			    String sta = text_Sta.getText();
			    String zip = text_Zip.getText();
			    String tel = text_Tel.getText();
			    String em = text_Em.getText();
			    
		  		book.addAddress(new AddressEntry(fn, ln, str, ct, sta, zip, tel, em));
			}
    	}
    	else if ("remove".equals(evt.getActionCommand()))
    	{
    		//Delete an Address Entry, Inform the user of deletion or not.
			if(book.deleteAddress())
				System.out.println("\n Entry sucessfully deleted.\n");
			else 
				System.out.println("\nNo Entries Deleted.\n");
    	}
    	else if ("find".equals(evt.getActionCommand()))
    	{
    		//Prompt the user for a last name to search on
			System.out.println("\nEnter a Last Name to Find\n");
			//Capture input
			String lastName = textField.getText();
			//Find a user specified AddressEntry by lastName
			book.find(lastName);	
    	}
    	else if ("list".equals(evt.getActionCommand()))
    	{
    		book.list();
    	}
    	else if ("exit".equals(evt.getActionCommand()))
    	{
    		book.store();
			System.out.print(Menu.exitMessage());
			System.exit(0);
    	}
}

    /**
     * Create the GUI and show it.  For thread safety,
     * this method should be invoked from the
     * event dispatch thread.
     */
    private static void createAndShowGUI() {
        //Create and set up the window.
        JFrame frame = new JFrame("TextDemo");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //Add contents to the window.
        frame.add(new AddressBookApplication());

        //Display the window.
        frame.pack();
        frame.setVisible(true);
    }

    public static void main(String[] args) 
    {
        //Schedule a job for the event dispatch thread:
        //creating and showing this application's GUI.
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run(){createAndShowGUI();}
        });
    }
}