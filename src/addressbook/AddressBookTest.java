package addressbook;

import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;

import org.junit.Test;

/**
 * Test driver for the AddressBook Class
 * @author Trey Freeman
 *
 */
public class AddressBookTest {
	
	/**
	 * Instance of AddressBook class used for testing
	 */
	private AddressBook testBook = new AddressBook();
	/**
	 * First fixin that can be used for testing
	 */
	private AddressEntry fixin = new AddressEntry("FirstName", "LastName","Street","City","State",
			  "Zip", "Telephone", "Email@email.com");
	
	/**
	 * Test loading from a file
	 */
	@Test
	public void testLoadFromFile() {
		testBook.loadFromFile("test.txt");
		assertTrue(testBook.find("test"));
	}
	
	/**
	 * Test adding an address to the addresses list
	 */
	@Test
	public void testAddAddress() {
		this.testBook.addAddress(this.fixin);
		assertTrue(testBook.find("LastName"));
	}
	
	/**
	 * Test deleting address from addresses list
	 */
	@Test
	public void testDeleteAddress() {
		this.testBook.addAddress(this.fixin);
		String simulatedUserInput = "LastName" + System.getProperty("line.separator") + "0" + 
									System.getProperty("line.separator") + "y";
		
		//Create custom input stream
		InputStream testInputStream = System.in;
		System.setIn(new ByteArrayInputStream(simulatedUserInput.getBytes()));
		
		//Delete the entry using the custom input stream
		this.testBook.deleteAddress();
		System.setIn(testInputStream);
		
		//Make sure it is gone
		assertFalse(this.testBook.find("test"));
	}
	
	/**
	 * Test listing out all the elements
	 */
	@Test
	public void testList() {
		//Add an address first
		this.testBook.addAddress(this.fixin);
		//Check that it has added and at least one is in the list
		assertTrue(this.testBook.list());
	}
	
	/**
	 * Test finding an AddressEntry by lastName
	 */
	@Test
	public void testFind() {
		//Add an address to test
		this.testBook.addAddress(this.fixin);
		//Check that it was added
		assertTrue(this.testBook.find("LastName"));
	}
	
	/**
	 * Test writing the AddressBook to a file.
	 */
	@Test
	public void testStore() {
		//Add at least one AddressEntry to the book
		this.testBook.addAddress(this.fixin);
		//Store output as testOutput.txt
		String simulatedUserInput = "testOutput.txt" + System.getProperty("line.separator"); 
		
		//Create custom input stream
		InputStream testInputStream = System.in;
		System.setIn(new ByteArrayInputStream(simulatedUserInput.getBytes()));
		
		//Delete the entry using the custom input stream
		this.testBook.store();
		System.setIn(testInputStream);
		
		//Check that the file now exists
		File file = new File("testOutput.txt");
		assertTrue(file.exists());;
	}

}