package addressbook;

import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Test class for AddressEntry
 * @author Trey Freeman
 *
 */
public class AddressEntryTest {
	
	/**
	 * Create fixin to be used to test different methods
	 */
	AddressEntry fixin = new AddressEntry("FirstName", "LastName","Street","City","State",
				 						  "Zip", "Telephone", "Email@email.com");
	/**
	 * Test getFirstName()
	 */
	@Test
	public void testGetFirstName() {
		assertEquals(fixin.getFirstName(), "FirstName");
	}
	
	/**
	 * Test getLastName()
	 */
	@Test
	public void testGetLastName() {
		assertEquals(fixin.getLastName(), "LastName" );
	}

	/**
	 * Test getStreet()
	 */
	@Test
	public void testGetStreet() {
		assertEquals(fixin.getStreet(), "Street" );
	}

	/**
	 * Test getCity()
	 */
	@Test
	public void testGetCity() {
		assertEquals(fixin.getCity(), "City");
	}

	/**
	 * Test getState()
	 */
	@Test
	public void testGetState() {
		assertEquals(fixin.getState(), "State");
	}

	/**
	 * Test getZip()
	 */
	@Test
	public void testGetZip() {
		assertEquals(fixin.getZip(), "Zip");
	}

	/**
	 * Test getTelephone()
	 */
	@Test
	public void testGetTelephone() {
		assertEquals(fixin.getTelephone(),"Telephone");
	}
	
	/**
	 * Test getEmail()
	 */
	@Test
	public void testGetEmail() {
		assertEquals(fixin.getEmail(), "Email@email.com");
	}
	
	/**
	 * Test setFirstName()
	 */
	@Test
	public void testSetFirstName() {
		fixin.setFirstName("Test");
		assertEquals(fixin.getFirstName(), "Test");
	}

	/**
	 * Test setLastName()
	 */
	@Test
	public void testSetLastName() {
		fixin.setLastName("Test");
		assertEquals(fixin.getLastName(), "Test");
	}
	
	/**
	 * Test setStreet()
	 */
	@Test
	public void testSetStreet() {
		fixin.setStreet("Test");
		assertEquals(fixin.getStreet(), "Test");
	}
	
	/**
	 * Test setCity()
	 */
	@Test
	public void testSetCity() {
		fixin.setCity("Test");
		assertEquals(fixin.getCity(), "Test");
	}
	
	/**
	 * Test setState()
	 */
	@Test
	public void testSetState() {
		fixin.setState("Test");
		assertEquals(fixin.getState(), "Test");
	}

	/**
	 * Test setZip()
	 */
	@Test
	public void testSetZip() {
		fixin.setZip("Test");
		assertEquals(fixin.getZip(), "Test");
	}
	
	/**
	 * Test setTelephone()
	 */
	@Test
	public void testSetTelephone() {
		fixin.setTelephone("Test");
		assertEquals(fixin.getTelephone(), "Test");
	}
	
	/**
	 * Test setEmail()
	 */
	@Test
	public void testSetEmail() {
		fixin.setEmail("Test@test.com");
		assertEquals(fixin.getEmail(), "Test@test.com");
	}
	
	/**
	 * Test default constructor for address entry. Will simulate user input.
	 */
	@Test
	public void testAddressEntry() {
		//Custom defined input stream
		String simulatedUserInput = "FirstName" + System.getProperty("line.separator")
	    + "LastName" + System.getProperty("line.separator") + "Street" + System.getProperty("line.separator")
	    + "City" + System.getProperty("line.separator") + "State" + System.getProperty("line.separator") 
	    + "Zip" + System.getProperty("line.separator") + "Telephone" + System.getProperty("line.separator")
	    + "Email@email.com" + System.getProperty("line.separator");
		
		//Creat custom input stream
		InputStream testInputStream = System.in;
		System.setIn(new ByteArrayInputStream(simulatedUserInput.getBytes()));
		
		//Test default constructor with user input
		AddressEntry test = new AddressEntry();
		System.setIn(testInputStream);
		
		//Check that all fields were set correctly
		assertEquals(test.getFirstName(), "FirstName");
		assertEquals(test.getLastName(), "LastName");
		assertEquals(test.getStreet(), "Street");
		assertEquals(test.getCity(), "City");
		assertEquals(test.getState(), "State");
		assertEquals(test.getZip(), "Zip");
		assertEquals(test.getTelephone(), "Telephone");
		assertEquals(test.getEmail(), "Email@email.com");
		
	}
	
	/**
	 * Test constructor that takes the 8 fields
	 */
	@Test
	public void testAddressEntryStringStringStringStringStringStringStringString() {
		AddressEntry test = new AddressEntry("FirstName", "LastName","Street","City","State",
											 "Zip", "Telephone", "Email@email.com");
		assertEquals(test.getFirstName(), "FirstName");
	}
	
	/**
	 * Test the pretty print output.
	 */
	@Test
	public void testToString() {
		assertTrue(fixin.toString().getClass().equals(String.class));
	}
	
	/**
	 * Test the file formatted output.
	 */
	@Test
	public void testToFile() {
		String fileFormat = fixin.toFile();
		String[] splitArray = fileFormat.split(",");
		assertEquals(splitArray.length, 8);
	}

}